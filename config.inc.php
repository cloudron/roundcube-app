<?php

$config['db_dsnw'] = getenv('CLOUDRON_MYSQL_URL');
// This also hides the host chooser in the login screen
$config['default_host'] = 'ssl://' . getenv('CLOUDRON_EMAIL_IMAP_SERVER') . ':' . getenv('CLOUDRON_EMAIL_IMAPS_PORT');
// Create the user, if not found in roundcube's db
$config['auto_create_user'] = true;
// If user logs in with just the username, append the domain below for IMAP login
$config['username_domain'] = getenv('CLOUDRON_EMAIL_DOMAIN');
$config['smtp_server'] = getenv('CLOUDRON_EMAIL_SMTP_SERVER') . ':' . getenv('CLOUDRON_EMAIL_SMTP_PORT');
$config['smtp_user'] = '%u'; // current imap username
$config['smtp_pass'] = '%p'; // current imap password
$config['identities_level'] = 0; // multiple identities with possibility to edit all parameters
$config['support_url'] = '';
$config['log_dir'] = '/tmp/roundcube/logs/';
$config['temp_dir'] = '/tmp/roundcube/temp/';
$config['des_key'] = '${des_key}';
$config['default_list_mode'] = 'threads';

$config['plugins'] = array('acl', 'archive', 'attachment_reminder', 'emoticons', 'managesieve', 'markasjunk', 'newmail_notifier', 'vcard_attachments', 'zipdownload');

// Set the Cloudron archive and junk folders
$config['archive_mbox'] = 'Archive';
$config['junk_mbox'] = 'Spam';

$config['managesieve_host'] = getenv('CLOUDRON_EMAIL_SIEVE_SERVER');
$config['managesieve_port'] = (int) getenv('CLOUDRON_EMAIL_SIEVE_PORT');
$config['managesieve_usetls'] = true; // this enables STARTTLS
$config['managesieve_mbox_encoding'] = 'UTF-8'; // use UTF-8 encoding when saving sieve scripts
$config['managesieve_vacation'] = 1; // adds a separate vacation section
$config['managesieve_forward'] = 1; // adds as separate forward section (redirect and copy to)

$config['managesieve_conn_options'] = array(
  'ssl'         => array(
     'verify_peer'      => false,
     'verify_peer_name' => false,
     'allow_self_signed'=> true
   ),
);

$config['smtp_conn_options'] = array(
  'ssl'         => array(
     'verify_peer'      => false,
     'verify_peer_name' => false,
     'allow_self_signed'=> true
  ),
);

$config['imap_conn_options'] = array(
  'ssl'         => array(
     'verify_peer'      => false,
     'verify_peer_name' => false,
     'allow_self_signed'=> true
  ),
);

// system error reporting, sum of: 1 = log; 4 = show
// $config['debug_level'] = 4;
// $config['managesieve_debug'] = true;
// $config['imap_debug'] = true;
// $config['ldap_debug'] = true;
// $config['smtp_debug'] = true;

require_once "/app/data/customconfig.php";

