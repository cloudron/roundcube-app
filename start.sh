#!/bin/bash

set -eu

mkdir -p /run/roundcube/sessions /run/roundcube/config /tmp/roundcube/temp /tmp/roundcube/logs

# init the database if needed
if [[ $(mysql -u${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} -h${CLOUDRON_MYSQL_HOST} -P${CLOUDRON_MYSQL_PORT} -D${CLOUDRON_MYSQL_DATABASE} -e "SHOW TABLES LIKE 'session'") ]]; then
    echo "Database already initialized"
else
    echo "Initializing database"
    mysql -u${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} -h${CLOUDRON_MYSQL_HOST} -P${CLOUDRON_MYSQL_PORT} -D${CLOUDRON_MYSQL_DATABASE} < ./SQL/mysql.initial.sql
fi

if [[ ! -f "/app/data/des_key" ]]; then
    echo "Generating DES key for new installation"
    des_key=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 24 | head -n 1)
    echo "${des_key}" > /app/data/des_key
else
    echo "Using existing DES key"
    des_key=$(cat /app/data/des_key)
fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

if [[ ! -f /app/data/customconfig.php ]]; then
    echo -e "<?php\n\n// Place custom configs here\n\n" > /app/data/customconfig.php
fi

mkdir -p /app/data/plugins
# the buit-in plugins are auto-enabled
for f in $(ls /app/code/plugins.orig); do
    if ! [ -e "/app/data/plugins/$f" ]; then
        ln -s "/app/code/plugins.orig/$f" "/app/data/plugins/$f"
    fi
done

mkdir -p /app/data/skins
for f in $(ls /app/code/skins.orig); do
    if ! [ -e "/app/data/skins/$f" ]; then
        ln -s "/app/code/skins.orig/$f" "/app/data/skins/$f"
    fi
done

# run migration script in case we updated
# the "-v ?" means the script should figure the previous version on its own
gosu www-data:www-data php ./bin/update.sh -v ? -y

chown -R www-data.www-data /run/roundcube /tmp/roundcube /app/data

echo "Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
