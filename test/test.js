#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.MAILBOX1 || !process.env.MAILBOX2 || !process.env.PASSWORD) {
    console.log('MAILBOX1, MAILBOX2, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    let browser, app;

    const TEST_TIMEOUT = 100000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const MAILBOX1 = process.env.MAILBOX1;
    const MAILBOX2 = process.env.MAILBOX2;
    const MAIL_0_TO = MAILBOX2;
    const MAIL_0_SUBJECT = 'Test subject 0';
    const MAIL_0_CONTENT = 'Test content 0';
    const FILTER_NAME = 'SubjectHasCloudron';
    const FILTER_SUBJECT = 'Cloudron';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username) {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.id('rcmloginuser')), TEST_TIMEOUT);
        await browser.findElement(By.id('rcmloginuser')).sendKeys(username);
        await browser.findElement(By.id('rcmloginpwd')).sendKeys(process.env.PASSWORD);
        await browser.findElement(By.id('rcmloginsubmit')).click();
        await browser.wait(until.elementLocated(By.xpath('//a/span[contains(text(), "Compose")]')), TEST_TIMEOUT);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(5000); // takes sometime for 'Compose' to become active
        await browser.findElement(By.xpath('//a/span[contains(text(), "Logout")]')).click();
        await browser.wait(until.elementLocated(By.id('rcmloginuser')), TEST_TIMEOUT);
    }

    async function sendMail() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(5000); // takes sometime for 'Compose' to become active
        await browser.findElement(By.xpath('//a/span[contains(text(), "Compose")]')).click();
        await browser.wait(until.elementLocated(By.id('composebody')), TEST_TIMEOUT);
        await browser.findElement(By.xpath('//div[@id="compose_to"]//input')).sendKeys(MAIL_0_TO);
        await browser.findElement(By.xpath('//input[@name="_subject"]')).sendKeys(MAIL_0_SUBJECT);
        await browser.findElement(By.id('composebody')).sendKeys(MAIL_0_CONTENT);
        await browser.findElement(By.xpath('//button[contains(text(), "Send")]')).click();
        await browser.sleep(10000);
    }

    async function getMail()  {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//*[text()="' + MAIL_0_SUBJECT + '"]')), TEST_TIMEOUT);
    }

    async function addFilter() {
        await browser.get('https://' + app.fqdn + '/?_task=settings&_action=plugin.managesieve');
        await browser.sleep(5000); // give sometime for the filters to load
        await browser.findElement(By.xpath('//span[text()="Create"]')).click();
        await browser.sleep(5000);
        await browser.switchTo().frame(browser.findElement(By.name('filter-box')));
        await browser.findElement(By.xpath('//input[@id="_name"]')).sendKeys(FILTER_NAME);
        await browser.findElement(By.xpath('//*[@id="rule_target0_list"]/div/div/input')).sendKeys(FILTER_SUBJECT);
        await browser.findElement(By.xpath('//button[@value="Save"]')).click();
        await browser.sleep(5000);
        await browser.switchTo().defaultContent();
        await browser.wait(until.elementLocated(By.xpath('//td[contains(text(), "' + FILTER_NAME + '")]')), TEST_TIMEOUT);
    }


    async function getFilter() {
        await browser.get('https://' + app.fqdn + '/?_task=settings&_action=plugin.managesieve');
        await browser.wait(until.elementLocated(By.xpath('//td[contains(text(), "' + FILTER_NAME + '")]')), TEST_TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, MAILBOX1));
    it('can send mail', sendMail);
    it('add filter', addFilter);
    it('get filter', getFilter);
    it('can logout', logout);

    it('can login', login.bind(null, MAILBOX2));
    it('get mail', getMail);
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', function () { execSync('cloudron restore --app ' + app.id, EXEC_ARGS); });

    it('can login', login.bind(null, MAILBOX1));
    it('get filter', getFilter);
    it('can logout', logout);

    it('can login', login.bind(null, MAILBOX2));
    it('get mail', getMail);
    it('can logout', logout);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id, EXEC_ARGS); });

    it('can login', login.bind(null, MAILBOX1));
    it('get filter', getFilter);
    it('can logout', logout);

    it('can login', login.bind(null, MAILBOX2));
    it('get mail', getMail);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);

    it('can login', login.bind(null, MAILBOX1));
    it('get filter', getFilter);
    it('can logout', logout);

    it('can login', login.bind(null, MAILBOX2));
    it('get mail', getMail);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id net.roundcube.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, MAILBOX1));
    it('can send mail', sendMail);
    it('add filter', addFilter);
    it('get filter', getFilter);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login', login.bind(null, MAILBOX1));
    it('get filter', getFilter);
    it('can logout', logout);

    it('can login', login.bind(null, MAILBOX2));
    it('get mail', getMail);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
