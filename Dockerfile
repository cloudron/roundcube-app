FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=roundcube/roundcubemail versioning=semver
ARG ROUNDCUBE_VERSION=1.6.10

RUN curl -L https://github.com/roundcube/roundcubemail/releases/download/$ROUNDCUBE_VERSION/roundcubemail-$ROUNDCUBE_VERSION-complete.tar.gz | tar -xz --strip-components 1 -f - && \
    mv /app/code/plugins /app/code/plugins.orig && ln -s /app/data/plugins /app/code/plugins && \
    mv /app/code/skins /app/code/skins.orig && ln -s /app/data/skins /app/code/skins && \
    chown -R www-data:www-data /app/code

COPY config.inc.php /app/code/config/config.inc.php

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/roundcube.conf /etc/apache2/sites-enabled/roundcube.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

RUN a2enmod rewrite

# mod_php config
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 25M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 25M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/roundcube/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

RUN phpenmod mcrypt

COPY start.sh /app/pkg/start.sh

CMD [ "/app/pkg/start.sh" ]
