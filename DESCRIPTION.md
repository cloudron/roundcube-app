### About

Roundcube webmail is a browser-based multilingual IMAP client with an
application-like user interface. It provides full functionality you
expect from an email client, including MIME support, address book,
folder manipulation, message searching and spell checking.

### Features

* Multilingual capabilities
* Find-as-you-type address book
* Richtext/HTML message composing
* Searching messages and contacts
* Canned response templatesNew!
* Shared folders and ACL
* Full featured address book

