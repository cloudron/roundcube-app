# Roundcube Cloudron App

This repository contains the Cloudron app package source for [Roundcube](https://roundcube.net/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=net.roundcube.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id net.roundcube.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd roundcube-app

cloudron build
cloudron install
```

## Config Notes

* `default_host` - The IMAP server chosen to perform login on. This can be a map (PHP array)
  which contains a map of IMAP hosts to display string. When there is a map, a combo box
  is shown in the login page

* `username_domain` - The domain to append when user logs in with just the username.  Only for
   IMAP servers that require full e-mail addresses for login. Specify an array with
   'host' => 'domain' values to support multiple hosts (i.e when user puts a username and selects
   a domain from the select box append this domain address).

* `mail_domain` - The domain to append for new users. This can happen when user logs in via LDAP
   and roundcube only has a username to work with. This can be a map from IMAP hosts to domain names.

* `auto_create_user` - Create a user in the database if IMAP auth succeeds. If set to false, only
  people already in db can authenticate.

* `include_host_config` - When set to true, `<hostname>.inc.php` will be incuded for per domain
  config.

There is no built-in support for switching "accounts"/"mailboxes". This means that we cannot
support multi-domain on the same installation easily. One angle for the future might be to
pre-configure the "selector" in the login screen.

## Logs

Logs are sent to `/tmp/roundcube/logs`. The following configs can be enabed:

```
// system error reporting, sum of: 1 = log; 4 = show
$config['debug_level'] = 4;

// Log SQL queries
$config['sql_debug'] = true;

// Log IMAP conversation
$config['imap_debug'] = true;

// Log LDAP conversation
$config['ldap_debug'] = true;

// Log SMTP conversation
$config['smtp_debug'] = true;
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the app is fine.

```
cd roundcube-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

